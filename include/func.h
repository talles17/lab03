#ifndef FUNC_H
#define FUNC_H

		 
template <typename Tipo, typename inteiro = int>
void preenche_vetor (Tipo *vetor[], inteiro tamanho  ) ;

template <typename Tipo, typename inteiro = int>
void imprime_vetor (Tipo *vetor[], inteiro tamanho)  ;

template <typename Tipo, typename inteiro = int>
Tipo doVector (Tipo *vetor[], inteiro tamanho, inteiro x, Tipo valor) ;

template <typename Tipo, typename inteiro = int>
Tipo opMax (Tipo *v[], inteiro tamanho, Tipo valor ) ;

template <typename Tipo, typename inteiro = int>
Tipo opMin (Tipo *v[], inteiro tamanho, Tipo valor ) ;

template <typename Tipo, typename inteiro = int>
Tipo opSum (Tipo *v[], inteiro tamanho , Tipo valor ) ;

template <typename Tipo, typename inteiro = int>
Tipo opAvg (Tipo *v[], inteiro tamanho , Tipo valor) ;

template <typename Tipo, typename inteiro = int>
Tipo opHig (Tipo *v[], inteiro tamanho , Tipo valor ) ;

template <typename Tipo, typename inteiro = int>
Tipo opLow (Tipo *v[], inteiro tamanho , Tipo valor ) ;


#endif /* FUNC_H */