RM = rm -rf

CC = g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

CFLAGS = -Wall -pedantic -ansi -std=c++11 -I$(INC_DIR)

.PHONY: all clean debug 

all: doVector

debug: CFLAGS += -g -O0
debug: doVector


doVector: $(OBJ_DIR)/func.o $(OBJ_DIR)/main_exemplo.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'doVector' criado em  $(BIN_DIR)] +++"
	@echo "============="


$(OBJ_DIR)/func.o: $(SRC_DIR)/func.cpp $(INC_DIR)/func.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/main_exemplo.o: $(SRC_DIR)/main_exemplo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


clean: 
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*