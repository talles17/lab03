#include <iostream>
#include <ctime>
#include <cstdlib>

#include "func.h"

using std::cout ;
using std::endl ;
using std::cin ;


template <typename Tipo, typename inteiro = int>
void preenche_vetor (Tipo *vetor[], inteiro tamanho  ) {
	inteiro q ;
	cout << "Digite o maior valor a ser randomizado: " ;
	cin >> q ;
	srand ( (unsigned) time(0) ) ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		*vetor[ii] = rand() % (q) + 1 ;
	}
}

template <typename Tipo, typename inteiro = int>
void imprime_vetor (Tipo *vetor[], inteiro tamanho) {
	cout <<"Vetor: [" ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		cout << vetor[ii] <<", " ;
	}
	cout <<"]" << endl ;
}


template <typename Tipo, typename inteiro = int>
Tipo doVector (Tipo *vetor[], inteiro tamanho, inteiro x, Tipo valor) {
	Tipo saida ;

	switch (x) {
		case 1:
			saida = opMax (vetor, tamanho, valor) ;
			return saida ;

		case 2:
			saida = opMin(vetor, tamanho, valor) ;
			return saida ;

		case 3:
			saida = opSum(vetor, tamanho, valor) ;
			return saida ;
		
		case 4:
			saida = opAvg(vetor, tamanho, valor) ;
			return saida ;

		case 5:
			saida = opHig(vetor, tamanho, valor) ;
			return saida ;

		case 6:
			saida = opLow(vetor, tamanho, valor) ;
			return saida ;
		default:
			return -1 ;
	}
}

template <typename Tipo, typename inteiro = int>
Tipo opMax (Tipo *v[], inteiro tamanho, Tipo valor ) {
	Tipo mvalor = *v[0] ;
	for (int ii = 1 ; ii < tamanho ; ii++) {
		if (v[ii] > v[ii-1]) {
			mvalor = *v[ii] ;
		}
	}

	return mvalor ;
}

template <typename Tipo, typename inteiro = int>
Tipo opMin (Tipo *v[], inteiro tamanho, Tipo valor ) {
	Tipo mvalor = *v[0] ;
	for (int ii = 1 ; ii < tamanho ; ii++) {
		if (v[ii] < v[ii-1]) {
			mvalor = *v[ii] ;
		}
	}

	return mvalor ;
}

template <typename Tipo, typename inteiro = int>
Tipo opSum (Tipo *v[], inteiro tamanho , Tipo valor ) {
	Tipo soma = 0 ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		soma += *v[ii] ;
	}

	return soma ;
}

template <typename Tipo, typename inteiro = int>
Tipo opAvg (Tipo *v[], inteiro tamanho , Tipo valor ) {
	Tipo media = 0 ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		media += *v[ii] ;
	}

	media = media/3 ;

	return media ;
}

template <typename Tipo, typename inteiro = int>
Tipo opHig (Tipo *v[], inteiro tamanho , Tipo valor ) {
	Tipo quantidade = 0 ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		if (*v[ii] > valor) {
			quantidade++ ;
		}
	}

	return quantidade ;
}

template <typename Tipo, typename inteiro = int>
Tipo opLow (Tipo *v[], inteiro tamanho , Tipo valor ) {
	Tipo quantidade = 0 ;
	for (int ii = 0 ; ii < tamanho ; ii++) {
		if (*v[ii] < valor) {
			quantidade++ ;
		}
	}

	return quantidade ;
}


