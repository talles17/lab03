/**
 * @file	main.cpp
 * @brief	Arquivo principal do programa que realiza operacoes sobre um vetor
 *			de diferentes tipos de dados
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	28/03/2017
 * @date	30/03/2017
 */
#include <iostream>
#include <string>

#include "func.h"

using namespace std ;

// Realizar inclusao das respectivas bibliotecas

/** @brief Funcao principal */
int main() {
	
	string tipo_dados;				// Tipo de dados (int, float, double,
	cout << "Tipo de dados: ";
	cin >> tipo_dados;

	int tamanho;
	cout << "Informe a quantidade de elementos: ";
	cin >> tamanho;

	// Possiveis vetores a serem alocados dinamicamente conforme a necessidade
	int* vint = NULL;			// Vetor de inteiros
	float* vfloat = NULL;		// Vetor de decimais (float)
	double* vdouble = NULL;		// Vetor de decimais (double)
	string* vstring = NULL;		// Vetor de strings

	// Alocacao, preenchimento e impressao do vetor de acordo com o tipo
	if (tipo_dados == "int") {
		// Alocar dinamicamente o vetor de inteiros
		vint = new int[tamanho] ;
		// Preencher o vetor chamando funcao genérica
		preenche_vetor (&vint, tamanho) ;
		// Imprimir o vetor chamando funcao genérica
		imprime_vetor (&vint, tamanho) ;
	}

	else if (tipo_dados == "float") {
		// Alocar dinamicamente o vetor de inteiros
		vfloat = new float[tamanho] ;
		// Preencher o vetor chamando funcao genérica
		preenche_vetor (&vfloat, tamanho) ;
		// Imprimir o vetor chamando funcao genérica
		imprime_vetor (&vfloat, tamanho) ;
	} 

	else if (tipo_dados == "double") {
		// Alocar dinamicamente o vetor de inteiros
		vdouble = new double[tamanho] ;
		// Preencher o vetor chamando funcao genérica
		preenche_vetor (&vdouble, tamanho) ;
		// Imprimir o vetor chamando funcao genérica
		imprime_vetor (&vdouble, tamanho) ;
	}  

	// Executar as mesmas instrucoes para os demais tipos de dados

	// Apresentar opcoes de operacoes ao usuario
	cout << "Operacoes: " << endl ;
	cout << "(1) Maior valor" << endl ;
	cout << "(2) Menor valor" << endl ;
	cout << "(3) Soma dos elementos" << endl ;
	cout << "(4) Media dos elementos" << endl ;
	cout << "(5) Elementos maiores que um valor" << endl ;
	cout << "(6) Elementos menores que um valor" << endl ;
	cout << "(0) Sair" << endl ;

	int a ;
	do {
		
		// Opcao de operacao a ser escolhida pelo usuario
   		cout << "Digite a sua opcao: " ;
   		cin >> a ;   
		if (a != 0) {
			if (tipo_dados == "int") {
				int saida ;
				if (a == 5 || a == 6) {
					int valor ;
					cout << "Digite um valor: " ;
					cin >> valor ;
					saida = doVector(&vint, tamanho, a , valor);	
				}
        		else {
        			saida = doVector(&vint, tamanho, a , 0);
        		}
       		} 

       		else if (tipo_dados == "float") {
				float saida ;
				if (a == 5 || a == 6) {
					float valor ;
					cout << "Digite um valor: " ;
					cin >> valor ;
					saida = doVector(&vfloat, tamanho, a , valor);	
				}
        		else {
        			saida = doVector(&vfloat, tamanho, a , 0);
        		}
       		} 

       		else if (tipo_dados == "double") {
           		double saida ;
           		if (a == 5 || a == 6) {
					double valor ;
					cout << "Digite um valor: " ;
					cin >> valor ;
					saida = doVector(&vdouble, tamanho, a , valor);	
				}
        		else {
        			saida = doVector(&vdouble, tamanho, a , 0);
        		}
       		} 

       		else {
           		//doVector(vstring, tamanho, a , 0);
       		} 
   		}

	} while ( a != 0) ;
	
	cout << "Programa encerrado." << endl;
	
	return 0;
}
